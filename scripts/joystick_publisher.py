#!/usr/bin/env python
# Joystick publisher
# Reads a USB joystick using pygame, 
# and publishes its values in ROS topic /joy 
# It will keep publishing although no new data
# by Arif Rahman Dec 2018 

import pygame
import rospy
from sensor_msgs.msg import Joy
 

def talker():
    # Initialize the joystick
    pygame.init()
    pygame.joystick.init()
    joystick_count = pygame.joystick.get_count()
    if joystick_count < 1:
        print("!!!!!!!!!!!!!!!!!!!!!!!!!\nJOYSTICK NOT CONNECTED.\n!!!!!!!!!!!!!!!!!!!!!!!!!\n ")
        return
    else:
        joystick = pygame.joystick.Joystick(0)
        joystick.init() 

    # Setup the node
    pub = rospy.Publisher('joy', Joy, queue_size=1)
    rospy.init_node('joy_publisher', anonymous=False)
    rate = rospy.Rate(20) 
    rospy.loginfo("Joystick publisher started. ")

    # Loop
    while not rospy.is_shutdown():
        msg = Joy()
        # Get pygame events
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                return
        # Then read the joystick
        try:
            # Read all buttons and axes
            for j in range(0,joystick.get_numaxes()):
                msg.axes.append(joystick.get_axis(j))
                
            for j in range(0,joystick.get_numbuttons()):
                msg.buttons.append(joystick.get_button(j))

            # Then publish
            msg.header.stamp = rospy.Time.now()
            # print rospy.Time.now()
            pub.publish(msg)
        except:
            print("Could not read the joystick")

        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pygame.quit ()
        # pass

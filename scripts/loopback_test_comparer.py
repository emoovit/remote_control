#!/usr/bin/env python
# Loopback tester - comparer
# This node subscribes to both "sender" and "replier"
# Upon receiving any data on these two topics, 
# it will save the data and the time. 
# Then, it will loop though both saved data 
# and compare them. 
# It will calculate the time difference between 
# receiving the data from "sender", and receiving the data 
# from "replier". 
# It will then report the loopback time, and 
# raises an alert if this time exceeds the threshold. 
# by Arif Rahman Jan 2019 

import rospy
import time
from std_msgs.msg import Int16, Float32

class RingBuffer:
    def __init__(self, size):
        self.data = [0 for i in xrange(size)]

    def append(self, x):
        self.data.pop(0)
        self.data.append(x)

    def get(self):
        return self.data

keep_length = 5 # how much data points to keep
diff_thres = 0.5 # delay threshold, in secs

send_data = RingBuffer(keep_length)
reply_data = RingBuffer(keep_length)

def send_callback(x):
    global send_data
    send_data.append([x.data,rospy.Time.now()])

def reply_callback(x):
    global reply_data
    reply_data.append([x.data,rospy.Time.now()])

def talker():
    global send_data
    global reply_data
    global diff_thres
    # Setup the node
    rospy.init_node('loopback_test_comparer', anonymous=False)
    time.sleep(1) # give time for node to start
    pub = rospy.Publisher('loopback_time', Float32, queue_size=5)
    rospy.Subscriber('loopback_send', Int16, send_callback)
    rospy.Subscriber('loopback_reply', Int16, reply_callback)
    
    rate = rospy.Rate(2) 
    rospy.loginfo("Loopback test comparer started. ")

    # Loop
    while not rospy.is_shutdown():
        try:
            reply = reply_data.get()
            send = send_data.get()
            # print reply
            # print send
            for i in range(keep_length):
                for j in range(keep_length):
                    # print i, j
                    if reply[i][0] == send[j][0]:
                        # found match
                        # print(reply[i])
                        # print(send[j])
                        # calculate the time difference and print out
                        diff = (reply[i][1] - send[j][1]).to_sec()
                        pub.publish(diff)
                        print("Loopback: {0:.6f}s".format(diff))

                        if diff > diff_thres:
                            ROS_WARNING("Loopback time exceeds threshold!")
                        # return
        except:
            pass
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

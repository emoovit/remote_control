#!/bin/bash

# Start VPN server

# Ensure script is ran as root
ROOT_UID=0   # Only users with $UID 0 have root privileges.
E_NOTROOT=65
E_NOPARAMS=66

if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Must be root to run this script. Re-run with sudo."
  exit $E_NOTROOT
fi  

# If vpn is already running, just exit. 
if [ -n "$(ifconfig | grep tap0)" ]; then
	echo "OpenVPN is already running."

# if not, run it
else 
	echo "Starting openvpn"
	cd /etc/openvpn 
	openvpn ./server.conf
	# pid_openvpn=$!

	# echo "openvpn started"
	# echo $pid_openvpn
fi
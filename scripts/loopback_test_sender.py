#!/usr/bin/env python
# Loopback tester - sender
# This node publishes a number every 0.1s
# Run this on the main computer, 
# and the "replier" node on the other computer.
# Also run "comparer" on this computer. 
# by Arif Rahman Jan 2019 

import rospy
from std_msgs.msg import Int16
 

def talker():
    # Setup the node
    pub = rospy.Publisher('loopback_send', Int16, queue_size=1)
    rospy.init_node('loopback_test_sender', anonymous=False)
    rate = rospy.Rate(10) 
    rospy.loginfo("Loopback test sender started. ")

    # Loop
    msg = Int16()
    msg = 0
    while not rospy.is_shutdown():
        pub.publish(msg)
        print msg
        msg+=1
        if msg > 9:
            msg=0
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

#!/usr/bin/env python
# Loopback tester - replier
# This node subscribes to "sender", 
# and simply publishes the same data. 
# Run the "sender" on the main computer, 
# and this node on the other computer.
# by Arif Rahman Jan 2019 

import rospy
from std_msgs.msg import Int16
 
data = Int16()
data = 0
old_data = Int16()
old_data = 0

def test_callback(x):
    global data
    data = x.data

def talker():
    global data
    global old_data
    # Setup the node
    pub = rospy.Publisher('loopback_reply', Int16, queue_size=1)
    rospy.Subscriber('loopback_send', Int16, test_callback)
    rospy.init_node('connection_test_replier', anonymous=False)
    rate = rospy.Rate(20) 
    rospy.loginfo("Loopback test replier started. ")

    # Loop
    while not rospy.is_shutdown():
        if data != old_data:
            pub.publish(data)
            print data 
        # msg+=1
        old_data = data
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

#!/usr/bin/env python
# import pygame
# import socket
import struct
# import binascii
import time
# import serial
# import re, redis
# import math
import can
# import numpy as np
import rospy
# import std_msgs.msg
# from geometry_msgs.msg import Point32
# from sensor_msgs.msg import PointCloud
# from radar_msgs.msg import radar_array
# from radar_msgs.msg import radar
# from visualization_msgs.msg import Marker
# from visualization_msgs.msg import MarkerArray
# import random
# import tf
from sensor_msgs.msg import Joy

DEBUG = True

if DEBUG:
    print('Debug mode')
    SEND = False
else:
    bus = can.Bus(interface='socketcan',
            bustype='socketcan_ctypes',
            channel='can0',
            bitrate="500000",
            receive_own_messages=False)

    canID  = 0x100  #0x100 TI
    SEND = True
    
joystick = Joy()
joystick.axes = [0.0, 0.0, 0.0, 0.0]
joystick.buttons = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


def joy_callback(data):
    global joystick 
    joystick = data


#Initilization
brake_percent = 0
throt_percent = 0
steer_ang = 540 #centre steering 0 - 1080


GAS_BRAKE = 0.0
STEER = 0.0
IS_SAFE = 0
BRAKE = 0
GAS = 0
_steer = 0
_id = 123  # llc msg id
MAX_STEER = 540
MAX_BRAKE = 100 # percentage
MAX_GAS = 50 # percentage
LLC_SEND_FREQ = 0.3 # 300 ms
SAFE_BRAKE_VAL =  60

# pygame.init()
rospy.init_node('remote_control', anonymous=False)
rospy.Subscriber('joy', Joy, joy_callback)

# _ts = int(time.time()*1000)

# -------- Main Program Loop -----------p
while not rospy.is_shutdown():
    # Check if data is older than 3 secs
    # if yes, then apply the brake
    if (rospy.Time.now() - joystick.header.stamp) > rospy.Duration(3):
        print "\n!!!!!\nDATA NOT RECEIVED. BRAKING VEHICLE.\n!!!!!\n"
        joystick.axes = [0.0, 0.0, 0.0, 0.0]
        joystick.buttons = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]

    # Process the joystick data
    GAS_BRAKE = joystick.axes[1]  # get value from axes 1
    STEER = joystick.axes[2]  # get value from axes 2
        
    if (joystick.buttons[4]) == 1 or (joystick.buttons[5]) == 1:  # Safety check Either buttons 4 or buttons 5 should be pressed to control gas, otherwise will brake
        IS_SAFE = 1
    else:
        IS_SAFE = 0
        
    
    if (GAS_BRAKE > 0):  # if axes goes up will GAS 
        BRAKE = int(GAS_BRAKE*MAX_BRAKE)
        GAS = 0
    else: # Axis goes down will brake
        GAS = int(GAS_BRAKE*-MAX_GAS)
        BRAKE = 0

    if (IS_SAFE == 0 and GAS > 0): #  if buttons not pressed will brake
        BRAKE = SAFE_BRAKE_VAL
        GAS = 0

    if(STEER <= 0):
        _sign = 0
    else:
        _sign = 1

    if (joystick.buttons[6]): # if buttons 6 pressed will brake SAFE BRAKE VALUE 
        BRAKE = SAFE_BRAKE_VAL

    _steer = int(abs(STEER*540))
    
    # print ((int(time.time()*1000) - _ts), _sign, _steer, BRAKE, GAS)
    
    
    #_values = (int(_id), int(_sign), int(_steer), BRAKE, GAS) #encode int into bytes
    #_structure = struct.Struct('< ' + 'IIIII')
    #_data = _structure.pack(*_values)
    #llc_sock.sendto(_data, (LLC_IP, LLC_CMD_PORT)) # send to LLC
    
    #_ts = int(time.time()*1000)
    if _sign == 0:
    	_llc_steer = MAX_STEER - _steer 
    else:
        _llc_steer = _steer + MAX_STEER

    #_cmd = str(GAS)+","
    #_cmd += str(int(_llc_steer))+","
    #_cmd += str(BRAKE)+"e"

    status = 1
    brake_com = 1
    
    if brake_com == 1:
        brake_percent = BRAKE
    else:
        brake_percent = 0
    throt_com = 1
    if throt_com == 1:
        throt_percent = GAS
    else:
        throt_percent = 0

    steer_com = 1
    if steer_com == 1:
        steer_ang = int(_llc_steer)
    else:
        steer_ang = 540

    # print steer_ang0, steer_ang1
    steer_ang_byte = map(hex,struct.unpack('>4B',struct.pack('>L',steer_ang))) #convert decimal to hex and in bytearray
    # byte in decimal 0 - 255
    byte0 = status          #status VI    
    byte1 = brake_com       #brake command 0,1
    byte2 = brake_percent   #brake percentage 0-100
    byte3 = throt_com       #throttle command 0,1
    byte4 = throt_percent   #throttle percentage 0-100
    byte5 = steer_com       #steering command 0,1
    byte6 = int(steer_ang_byte[2],0)        #steering angle 1 0-1080
    byte7 = int(steer_ang_byte[3],0)        #steering angle 2

    

    print "BRAKE: \t"+str(brake_percent)+"\t; THROTTLE: \t"+str(throt_percent)+"\t; STEER: \t"+str(steer_ang)

    if SEND:
        try:
            _smcmd = can.Message(arbitration_id=canID, data=[byte0, byte1, byte2, byte3, byte4, byte5, byte6, byte7], extended_id=False) #Hex decimal value
            bus.send(_smcmd)

        except ValueError as err:
            print err
        except IndexError as err:
            print err
        except KeyboardInterrupt:
            exit()
            
        #cmd =  _cmd
        #send_freq = int(current_milli_time() - _last_time)
        #_last_time = current_milli_time()
    else:
        print "Status: "+str(status)+" ; SEND: "+str(SEND)+" ; Check TI connections"

    time.sleep(0.001)


#!/bin/bash

export ROS_MASTER_URI=http://10.8.0.2:11311
export ROS_HOSTNAME=10.8.0.2
sleep 1

source ~/catkin_ws/devel/setup.bash
rosrun remote_control teleop_car.py
